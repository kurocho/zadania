import time

def CanReview(table):
    converted = list()
    for e in table:
        converted.append(time.strptime(e, '%Y-%m-%d %H:%M:%S'))
    count = 1
    newSessionCount = 1
    for i in range(1,len(converted)):
        if (converted[i].tm_yday - converted[i-1].tm_yday) == 1:
            count += 1
            if ChangeToSeconds(converted[i]) - ChangeToSeconds(converted[i-1]) >= 1800: # licze co do sekduny

                newSessionCount +=1
            else:
                newSessionCount = 1

        elif (converted[i].tm_yday - converted[i-1].tm_yday) == 0:
            if ChangeToSeconds(converted[i]) - ChangeToSeconds(converted[i-1]) >= 1800: # licze co do sekduny

                newSessionCount +=1
            else:
                newSessionCount = 1
        else:
            if(count >= 3 and newSessionCount >= 6):
                return True
            else:
                count = 1
                newSessionCount = 1

    if(count >= 3 and newSessionCount >= 6):
        return True
    else:
        return False


def ChangeToSeconds(myStruct):
    if myStruct.tm_year % 4 == 0: # sprawdzam rok przestepny
        leapYear = 366
    else:
        leapYear = 365
    return myStruct.tm_year * leapYear * 24 * 60 * 60 + myStruct.tm_yday * 24 * 60 * 60+ myStruct.tm_hour * 60 * 60 + myStruct.tm_min * 60 + myStruct.tm_sec

A = ['2017-03-10 08:13:11', '2017-03-10 19:01:27', '2017-03-11 07:35:55', '2017-03-11 16:15:11', '2017-03-12 08:01:41', '2017-03-12 17:19:08']
B = ['2017-03-10 18:58:11', '2017-03-10 19:01:27', '2017-03-11 07:35:55', '2017-03-11 16:15:11', '2017-03-12 08:01:41', '2017-03-12 17:19:08']
C = ['2017-03-08 17:11:13', '2017-03-11 17:22:47', '2017-03-11 19:23:51', '2017-03-11 22:03:12', '2017-03-12 08:32:04', '2017-03-12 13:19:08', '2017-03-12 17:19:08']
D = ['2017-03-14 13:19:08', '2017-03-14 17:19:08' , '2017-03-15 12:19:08', '2017-03-16 12:19:08', '2017-03-16 23:49:08', '2017-03-17 00:10:08']
print(CanReview(A))
print(CanReview(B))
print(CanReview(C))
print(CanReview(D)) # przypadek gdy dzien sie rozni jednak nowa sesja wystepuje wczesniej niz 30 min

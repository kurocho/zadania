import math
def MyFunction(A,B):
    C = list()
    countArray=[0 for _ in range(max(B)+1)]
    for e in B:
        countArray[e]+=1

    for e in A:
        if not IsPrime(countArray[e]):
            C.append(e)

    return C


def IsPrime(number):
    numberSqrt = (int)(math.sqrt(number))
    if(number == 0 or number == 1):
        return False
    if(number == 2):
        return True
    for i in range(2,numberSqrt+1):
        if number%i==0:
            return False
    return True

A=[2,3,9,2,5,1,3,7,10]
B=[2,1,3,4,3,10,6,6,1,7,10,10,10]
print(MyFunction(A,B))

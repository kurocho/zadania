def insertSort(l): # pesymistycznie O(n^2) pamieciowa O(1) stabilny
    dlugosc = len(l)
    for i in range(1,dlugosc):
        for j in range(0,i):
            if(l[i]<l[j]):
                l.insert(j,l.pop(i))
                break

mojalista = [454,3,4,5,3,23,23,2,41,412,4,23,5432,4,1241]
insertSort(mojalista)
print(mojalista)

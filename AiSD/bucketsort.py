def bucketSort(array):
    buckets = [[] for _ in len(array)]
    for i in array:
        buckets[int(0.1*array[i])].append(array[i])

    for e in buckets:
        insertSort(e)

    bElements = 0
    for b in buckets:
        for i in range(len(buckets[b])):
            array[bElements + i] = buckets[b][i]
        bElements += len(buckets[b])

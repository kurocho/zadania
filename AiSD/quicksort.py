def quickSort(tab,left,right): #O(n logn) - srednio O(n^2) - pesymistycznie niestabilny
    i=left
    j=right
    p=tab[(i+j)//2]
    while(i<=j):
        while(tab[i]<p):
            i+=1
        while(tab[j]>p):
            j-=1
        if(i<=j):
            tab[i], tab[j] = tab[j], tab[i]
            i+=1
            j-=1
    if(j>left):
        QuickSort(tab,left,j)
    if(i<right):
        QuickSort(tab,i,right)

import math

def merge(array, start, mid, end): # O(n logn) a pamieciowa O(n) stabiny
    left_array = [array[i] for i in range(start, mid)]
    right_array = [array[i] for i in range(mid, end)]
    guard = math.inf
    left_array.append(guard)
    right_array.append(guard)
    left_index = 0
    right_index = 0
    total_elements = len(left_array) + len(right_array) - 2

    for i in range(0, total_elements):
        if left_array[left_index] <= right_array[right_index]:
            array[start + i] = left_array[left_index]
            left_index += 1
        else:
            array[start + i] = right_array[right_index]
            right_index += 1

def mergeSort(array, start, end):
    if start + 1 < end:
        mid = (start + end) // 2
        merge_sort(array, start, mid)
        merge_sort(array, mid, end)
        merge(array, start, mid, end)

array = [3,1,4,2]
mergeSort(array, 0, len(array))
print(array)

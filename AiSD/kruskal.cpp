class Element:
    def __init__(self, value):
        self.value = value
        self.parent = self
        self.depth = 0

def make_set(value):
    return Element(value)

# Find(Element) -> Set
def find(element):
    if element.parent == element:
        return element
    element.parent = find(element.parent)
    return element.parent

# Union(Set1, Set2) -> Set
def union(el1, el2):
    set1 = find(el1)
    set2 = find(el2)
    if set1 == set2:
        return

    if set1.depth < set2.depth:
        set1.parent = set2
    elif set1.depth > set2.depth:
        set2.parent = set1
    else:
        set2.parent = set1
        set1.depth += 1

def kruskal(vertices, edges):
    tree = set()
    elements = dict()
    for v in vertices:
        elements[v] = make_set(v)

    sorted_edges = sorted(edges, key = lambda x: x[2])
    for (s, e, w) in sorted_edges:
        el1 = elements[s]
        el2 = elements[e]
        if find(el1) != find(el2):
            tree.add((s, e, w))
            union(el1, el2)

    return tree

print(kruskal([1,2,3,4,5], [(2, 3, 3),(1, 2, 1),(3, 5, 1),(1, 3, 2),(3, 4, 2)]))

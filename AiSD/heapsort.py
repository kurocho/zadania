def HeapSort(l): # O(n logn) a pamieciowa O(n) jest niestabliny
    heapLen = len(l) - 1
    half = heapLen // 2
    for i in range(half, -1, -1):
        Heapify(l,i,heapLen)

    for i in range(heapLen, 0, -1):
        if l[0]>l[i]:
            l[0], l[i] = l[i], l[0]
            Heapify(l,0,i-1)

def Heapify(l, first, last):
  maxEl = 2*first + 1
  while(maxEl <= last):
    if(maxEl < last) and (l[maxEl] < l[maxEl+1]):
        maxEl+=1

    if l[maxEl] > l[first]:
        l[maxEl], l[first] = l[first], l[maxEl]
        first = maxEl;
        maxEl = 2*first + 1
    else:
        break


mojalista = [3,4,5,6,434,45,53,343,42,131,131,3,4,5,34,32]
HeapSort(mojalista);
print(mojalista)

def countSort(array, max_element):# O(n+k) a pamieciowa O(n+k) stabilny
    count_array=[0 for _ in range(max_element)]
    result=[0 for _ in range(len(array))]

    for e in array:
        count_array[e]+=1
    for i in range(1,max_element):
        count_array[i]=count_array[i]+count_array[i-1]
    for i in range(len(array)-1,-1,-1):
        e=array[i]
        result[count_array[e]-1]=e
        count_array[e]-=1
    return result

tab=[5,4,3,3,7,9]
tab = countSort(tab,10)
print(tab)

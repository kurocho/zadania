#include <iostream>
#include <string>
#include <list>
#include <map>

using namespace std;
//******************************************************
void cin_data(map<string, string> & dictionary);
void insert_data(map<string, string> & dictionary, string pol, string eng);
void print_dict(map<string,string> & dictionary);
bool hasnumbers(string text);
string translate_to_eng(map<string,string> & dictionary, string);
string translate_to_pol(map<string,string> & dictionary, string);
//******************************************************
int main(int argc, char const *argv[]) {

    // list<string> o1;
    // o1.push_back("b");
    // o1.push_back("a");
    // o1.sort();
    // for(list<string>::iterator i = o1.begin(); i!= o1.end();i++)
    //     cout << *i;
    //
    // map<string,int> o2;
    // o2["cos"] = 5;


    /* slownik na mapach */
    map<string,string> dictionary;

    try{
        insert_data(dictionary,"pies","dog");
        insert_data(dictionary,"kot","cat");
        insert_data(dictionary,"koń","horse");
        // cin_data(dictionary);
        // cin_data(dictionary);
        // cin_data(dictionary);
        cout << translate_to_eng(dictionary,"kot") << endl;
        cout << translate_to_pol(dictionary,"cat") << endl;
    }catch(int err){
        /*
            443 - za krotkie slowo
            444 - zawiera liczby w tekscie
            555 - brak slowa w slowniku

        */
        cout << "Error: " << err << endl;
    }

    print_dict(dictionary);

}
/*                 Definicje Funckji                   */
//******************************************************
void cin_data(map<string, string> & dictionary){
    string pol,eng;
    cout << "Wpisz słowo po polsku: ";
    cin >> pol;
    cout << "Wpisz słowo po angielsku: ";
    cin >> eng;
    insert_data(dictionary,pol,eng);
}

void insert_data(map<string, string> & dictionary, string pol, string eng){
    if(pol.length()<=0 || eng.length()<=0)
        throw 443;
    else
        if(hasnumbers(pol) || hasnumbers(eng))
            throw 444;
        else{
            dictionary[pol] = eng;
        }

}

void print_dict(map<string,string> & dictionary){
    for(map<string,string>::iterator i = dictionary.begin();i!=dictionary.end();i++)
        cout << i->first << " " << i->second << endl;
}

bool hasnumbers(string text){
    int len = text.length();
    for(int i = 0;i<text.length();i++){
        if((int)text[i]<= 57 && (int)text[i]>=48)
            return true;
    }
    return false;
}

/* Szukanie w slowniku */

string translate_to_eng(map<string,string> & dictionary, string pol){
    for(map<string,string>::iterator i = dictionary.begin();i!=dictionary.end();i++){
        if(i->first == pol){
            return i->second;
        }
    }
    throw 555;
    return "";
}
string translate_to_pol(map<string,string> & dictionary, string eng){
    for(map<string,string>::iterator i = dictionary.begin();i!=dictionary.end();i++){
        if(i->second == eng){
            return i->first;
        }
    }
    throw 555;
    return "";
}

//******************************************************

#include <iostream>
#include <stdio.h>
#include "Macierz.cpp"
#include "complex.cpp"
using namespace std;

template<class Type>
class NODE
{
public:
    Type value;
    NODE<Type> *next;

    NODE(){
        //value=0;
        next=NULL;
    }
};
template<class Type>
class LIST{
    public:
    NODE<Type> *head;
    LIST()
    {
        //cout<<"Uruchomiono konstruktor listy"<<endl;
        head=NULL;
    }

    ~LIST()
    {
        NODE<Type> *tmp=head;
        cout<<"Uruchomiono destruktor listy"<<endl;
        while(tmp!=NULL)
        {
            //cout << tmp->value << endl; fflush(stdout);
            tmp=tmp->next;
            delete head;
            head=tmp;
        }
    }

    friend istream &operator>>(istream &s, LIST<Type> &l1)
    {
        NODE<Type> *tmp= new NODE<Type>;
        NODE<Type> *it;
        s>>tmp->value;
        if(l1.head==NULL)
        {
            l1.head=tmp;
        }
        else
        {
            it=l1.head;
            while(it->next!=NULL)
            {
                it=it->next;
            }
            it->next=tmp;
        }
        return s;
    }

    friend ostream &operator<<(ostream &s, LIST<Type> &l1)
    {
    	NODE<Type> *tmp;
        tmp=l1.head;
        while(tmp)
        {
            s<<tmp->value;
            tmp=tmp->next;
        }
        return s;
    }
    // bool operator==(const LIST<Type> &l1){
    //     NODE<Type> tmp1 = this->head;
    //     NODE<Type> tmp2 = l1.head;
    //     while(tmp1->next){
    //         if(tmp1.value!=tmp2.value)
    //             return 0;
    //         tmp1 = tmp1->next;
    //         tmp2 = tmp2->next;
    //     }
    //     return 1;
    // }
    LIST<Type> & operator=(const LIST<Type> &second)
    {
        cout << "Operator przypisania listy" << endl;
        if(this == &second){
            cout << "Listy takie same" << endl;
            return *this;
        }
        //cout << "destruktor w przypisaniu" << endl; fflush(stdout);
        this->~LIST();
        //cout << "koniec destruktora w przypisaniu" << endl; fflush(stdout);
        NODE<Type> *tmp, *tmp2, *copy;
        tmp=second.head;
        while(tmp!=NULL){
            copy=new NODE<Type>;
            copy->next = NULL;
            copy->value=tmp->value;
            if(this->head==NULL){
                head = copy;
            }
            else
            {
                tmp2->next = copy;
            }
            tmp2 = copy;
            tmp=tmp->next;
        }
        cout << "Koniec operatora przypisania listy" << endl;
        return *this;
    }

    LIST<Type> & operator+(LIST<Type> &second)
    {
        cout << "Operator dodawania" << endl;
        if(second.head == NULL)
            return *this;
        else if(this->head == NULL)
            return second;
        static LIST<Type> result;
        result = *this;
        NODE<Type> *it = result.head;
        while(it->next != NULL)
        {
            it = it->next;
        }
        NODE<Type> *second_wsk = second.head;

        while(second_wsk != NULL)
        {
            NODE<Type> *tmp = new NODE<Type>;
            tmp->next = NULL;
            tmp->value = second_wsk->value;
            it->next = tmp;
            it = tmp;
            second_wsk = second_wsk->next;
        }
        cout << "Operator dodawania koniec" << endl;

        return result;
    }

    int list_len()
    {
        NODE<Type> *tmp;
        int i=0;
        tmp=head;
        while(tmp->next)
        {
            i++;
            tmp=tmp->next;
        }
        return i;
    }

	void sort()
    {
        if(this->head!=NULL){
            int len=list_len();
            NODE<Type> *tmp = new NODE<Type>;
            for (int i=0;i<len;i++)
            {
                tmp=head;
                for (int j=0;j<=len-i-1;j++)
                {
                    if (tmp->next->value < tmp->value)
                    {
                        Type a=tmp->next->value;
                        tmp->next->value=tmp->value;
                        tmp->value=a;
                    }
                    tmp=tmp->next;
                }
            }
        }
    }
};




// int main(){
//     LIST <MACIERZ> L;
//     LIST <MACIERZ> N;
    // cin >> L;
    // // L.sort();
    // // cout << "L: \n"<< L;
    // cin >> N;
    // cout << "N:\n" << N;
    // LIST <MACIERZ> M;
    // M=L+N;
    // M.sort();
    // cout << "M:\n"<< M;
    // LIST <MACIERZ> a, b, c;
    // cin >> a >> a >> a;
    // cin >> b >> b;
    // c = a+b;
    // cout << "KONIEC 1 A + B" << endl;
    // c = a+b;
    // cout << "KONIEC 2 A + B" << endl;
    // cout << c;
    // c.sort();
    // cout << c;
    //
    //
    //
    // cout << "\n\n>>Koniec main zamykanie programu<<\n\n";
    return 0;
}

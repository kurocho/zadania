#include <iostream>
#include <cmath>
using namespace std;
//**************************************
class Node{
public:
    int value;
    Node *next;
    Node(int _value = 0){
        cout << "Node: Uruchomiono konstruktor" << endl;
        value = _value;
        next = NULL;
    }
    ~Node(){
        cout << "Node: Uruchomiono destruktor" << endl;
        value = 0;
        next = NULL;
    }
};
//**************************************
class List{
    Node *head;
public:
    List(){
        cout << "Lista: Uruchomiono konstruktor" << endl;
        head = NULL;
    }
    ~List(){
        cout << "Lista: Uruchomiono destruktor" << endl;
        Node *tmp = head;
        while(tmp!=NULL){
            tmp = tmp->next;
            cout << "Usuwam: " << head->value << endl;
            delete head;
            head = tmp;
        }
    }
    friend istream & operator>>(istream &s, List &myList){
        Node *ptr = myList.head;
        Node *tmp = new Node;
        cout << "Wpisz element: ";
        s >> tmp->value;
        tmp -> next = NULL;
        if(myList.head == NULL){
            myList.head = tmp;
        }
        else{
            while(ptr->next){
                ptr = ptr->next;
            }
            ptr -> next = tmp;
        }

        return s;


    }
    friend ostream & operator<<(ostream &s, List &existingList){
        Node *tmp = existingList.head;
        s << "Elementy: ";
        while(tmp){
            s << tmp->value << " ";
            tmp = tmp -> next;
        }
        s << "\n";
        return s;
    }
    List & operator=(const List &existingList){
        if(this == &existingList)
            return *this;
        Node *from = existingList.head;
        this->~List();
        if(existingList.head == NULL)
            return *this;
        // 1 element
        this->head = new Node;
        Node *to = this->head;
        to->value = from->value;
        to->next = NULL;
        // kolejne elementy
        while(from->next){
            to->next = new Node;
            to = to -> next;
            from = from -> next;
            to->value = from->value;
            to->next = NULL;

        }
        return *this;
    }
    List & operator+(List &existingList){
        if(!this->head)
            return existingList;
        if(!existingList.head)
            return *this;
        Node *to = this->head;
        Node *from = existingList.head;
        while(to->next){
            to = to->next;
        }
        Node *tmp = new Node;
        tmp->value = from->value;
        to->next = tmp;
        to = to->next;
        to->next = NULL;

        while(from->next){
            from = from->next;
            tmp = new Node;
            tmp->value = from->value;
            to->next = tmp;
            to = to->next;
            to->next = NULL;
        }
        return *this;
    }

    friend int list_size(List &existingList){
        if(!existingList.head)
            return 0;
        Node *ptr = existingList.head;
        int size = 1;
        while(ptr->next){
            size++;
            ptr = ptr ->next;
        }
        return size;
    }

    friend void bubble_sort(List &existingList){
        int size = list_size(existingList)-1;
        Node *ptr = existingList.head;
        Node *tmp;
        for(int i = 0 ;i<size;i++){
            for(int j = 0 ;j<size-i;j++){
                if(ptr->value > ptr->next->value){
                    tmp = new Node;
                    tmp->value = ptr->value;
                    ptr->value = ptr->next->value;
                    ptr->next->value = tmp->value;
                    delete tmp;
                }
                ptr = ptr->next;
            }
            ptr = existingList.head;
        }
    }

};


//**************************************
int main(int argc, char const *argv[]) {
    List mojasuperlista, drugasuperlista, trzeciasuperlista, czwartasuperlista;
    cin >> mojasuperlista;
    cin >> mojasuperlista;
    cin >> mojasuperlista;
    cin >> mojasuperlista;
    cin >> mojasuperlista;
    cin >> mojasuperlista;
    bubble_sort(mojasuperlista);
    cout << "Posorotowane:" << endl;
    cout << mojasuperlista;
    cout << " KONIEC " << endl;
    return 0;
}

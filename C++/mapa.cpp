#include <iostream>
#include <string>
#include "Macierz.cpp"
/* operatory: == , !=, ++
 funkcje begin i end  - zwracaja typ iterator
  nowa klasa para z operatorem strzałka i -> first i ->second ktory zwraca adres
  iterator musi zawierac pozycje w mapie i adres mapy
*/
using namespace std;
template <class Key, class Value>
class Mapa{
public:
    /*--------------------------------------------------------------*/
    class Para{
    public:
        Key first;
        Value second;
    };
    Para *tab;
    int size;
    /*--------------------------------------------------------------*/
    class Iterator{
    public:
        int pos;
        Mapa &map;
        Iterator(int index,Mapa &map = NULL):pos(index),map(map){}


        ~Iterator(){}

        Iterator & operator++(int){
            pos++;
            return *this;
        }
        bool operator==(const Iterator &m){
            return (this->pos==m.pos);
        }
        bool operator!=(const Iterator &m){
            return (this->pos!=m.pos);
        }
        Para *operator->(){
            return &map.tab[pos];
        }
    };
    /*--------------------------------------------------------------*/
    Mapa(Key n, Value m){
       tab = new Para[1];
       tab[0].first = n;
       tab[0].second = m;
       size=1;
    }
    Mapa(){
        size = 0;
        tab = new Para[0];
    }
    ~Mapa(){
        delete []tab;
    }

    Mapa & operator=(const Mapa &to_write){
        cout << "Operator przypisania mapy" << endl;
        if(to_write.tab==NULL){
            return *this;
        }
        this->~Mapa();
        this->size = to_write.size;
        tab = new Para[size];
        for(int i = 0; i<size;i++){
            tab[i] = to_write.tab[i];
        }
        return *this;
    }

    Key & operator[](Value _first){
        for(int i = 0; i<size; i++){
            if(tab[i].first == _first){
                return tab[i].second;
            }
        }
        Para *tmp = tab;
        size++;
        tab = new Para[size];
        for(int i=0;i<size-1;i++)
		{
			tab[i]=tmp[i];
		}
        tab[size-1].first = _first;
        return tab[size-1].second;

    }

    Iterator begin()
    {
        return Iterator(0, *this);
    }

    Iterator end()
    {
        return Iterator(size, *this);
    }
};
int main(int argc, char const *argv[]) {
    MACIERZ a,b,c,d;
    Mapa<MACIERZ,MACIERZ> M,G;
    cin >> a >> b >> c >> d;
    M[a] = b;
    M[c] = d;

    G=M;
    G=M;
    for(Mapa<MACIERZ,MACIERZ>::Iterator i = G.begin();i!=G.end();i++){
        cout << i->first << i->second << endl;

    }
    return 0;
}

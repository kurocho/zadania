#include <iostream>

using namespace std;

class czas{
    int godzina, minuta, sekunda;
public:
    void WprowadzCzas(int _g, int _m, int _s){
        godzina = _g;
        minuta = _m;
        sekunda = _s;
    }

    void WprowadzCzas(char *tekst){
        sscanf(tekst,"%d:%d:%d", &godzina, &minuta, &sekunda);
    }

    void OdczytajCzas(char *tekst){
        sprintf(tekst,"%d:%d:%d",godzina,minuta,sekunda);
    }
    void OdczytajCzas(int &g, int &m, int &s){
        g = godzina;
        m = minuta;
        s = sekunda;
    }

    bool operator<(czas o1){
        if((o1.godzina*3600+o1.minuta*60+o1.sekunda)<(godzina*3600+minuta*60+sekunda))
            return 1;
        else
            return 0;

    }
    bool operator==(czas o1){
        if((o1.godzina*3600+o1.minuta*60+o1.sekunda)==(godzina*3600+minuta*60+sekunda))
            return 1;
        else
            return 0;

    }

};
int main(int argc, char const *argv[]) {
    int z1,z2,z3;
    char tekst[256];
    czas dokladnagodzina, dokladnagodzina2;
    dokladnagodzina.WprowadzCzas(23,34,12);
    dokladnagodzina.OdczytajCzas(tekst);
    cout << tekst << endl;
    dokladnagodzina.WprowadzCzas("14:56:30");
    dokladnagodzina.OdczytajCzas(z1,z2,z3);
    cout << z1 << " " << z2 << " " << z3 << endl;
    dokladnagodzina2.WprowadzCzas("14:56:30");
    cout << (dokladnagodzina==dokladnagodzina2) << endl;
    return 0;
}

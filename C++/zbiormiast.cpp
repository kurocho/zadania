#include <string>
#include <vector>
#include <algorithm>
#include <iostream>

using namespace std;

class Miasto{
    string nazwa;
    double x;
    double y;
public:
    friend class ZbiorMiast;
    Miasto(string nazwa, double x, double y){
        this->nazwa = nazwa;
        this->x = x;
        this->y = y;
    }
    /*double getX()const {return x;}
    double getY()const {return y;}*/
};

class ZbiorMiast{
    vector<Miasto> miasta;
public:
    bool dodaj(const Miasto &m){
        for(vector<Miasto>::iterator it = miasta.begin();it!= miasta.end();it++){
            if(m.nazwa==it->nazwa){
                it->x = m.x;
                it->y = m.y;
                return true;
            }
        }
        miasta.push_back(m);
        return true;
    }
    void dodaj(ZbiorMiast &zbior2){
        for(int it = 0; it<zbior2.miasta.size();it++){
            const char *nazwa = zbior2.miasta[it].nazwa.c_str();
            if(!znajdz(nazwa))
                miasta.push_back(zbior2.miasta[it]);
        }
    }
    Miasto *znajdz(const char *name){
        for(int it = 0;it < miasta.size();it++){
            if(name==miasta[it].nazwa){
                return &miasta[it];
            }
        }
        return NULL;
    }

    void usunSpoza(ZbiorMiast &zbior2){
        for(vector<Miasto>::iterator it = miasta.begin(); it!=miasta.end();it++){
            const char *nazwa = it->nazwa.c_str();
            if(!zbior2.miasta.znajdz(nazwa))
                miasta.erase(it);
        }
    }
    void print(){
        for(vector<Miasto>::iterator it = miasta.begin();it!= miasta.end();it++){
            cout << it->nazwa << " " << it->x << " "<< it -> y << endl;;
        }
    }

};

int main(){
    ZbiorMiast z1;
    z1.dodaj(Miasto("Krosno",0,0));
    z1.dodaj(Miasto("Krakow",2,2));
    z1.dodaj(Miasto("Warszawa",3,2));
    z1.print();
    cout << endl;
    ZbiorMiast z2;
    z2.dodaj(Miasto("Krosno",1,1));
    z2.dodaj(Miasto("Stepina",3,4));
    z2.print();
    cout << endl;
    z1.dodaj(z2);
    z1.dodaj(Miasto("Stepina",2,2));
    z1.print();
    cout << endl;
    z1.usunSpoza(z2);
    z1.print();
}

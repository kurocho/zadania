/* - samodzielna rez pamięci
- 1 bajt to 1 cyfra
- dynamiczna rezerwacja
operatory >> , << , = , +
stop wpisywania dla np  \n, spacji
tablice znakow
operator * - w klasie pochodnej
*/
#include <iostream>
#include <string>
#include <stdio.h>
using namespace std;

int cti(char tmp){
    return tmp-48;
}
char itc(int tmp){
    return tmp+48;
}

class LARGE_NUMBERS{
protected:
    char *large_number_array;
    int number_length;
public:
    LARGE_NUMBERS(){
    //   cout << ">>CONSTRUCT<<" << endl;
        large_number_array = NULL;
        number_length = 0;
    }
    ~LARGE_NUMBERS(){
        //cout << ">>DESTRUCT<<" << endl;
        if(large_number_array!=NULL){
            delete []large_number_array;
            large_number_array = NULL;
            number_length = 0;
        }
    }

    LARGE_NUMBERS & cut_zero(int at = 0){
        if(cti(large_number_array[at])==0){
            LARGE_NUMBERS tmp = *this;
            large_number_array = new char[number_length-1];
            number_length--;
            memcpy(&large_number_array[0],&tmp.large_number_array[1],number_length);
            cut_zero(at);
        }
        return *this;
    }

    friend istream & operator>>(istream &p, LARGE_NUMBERS &new_number){
        string stream;
        p>>stream;
        new_number.number_length = stream.length();
        new_number.large_number_array = new char [new_number.number_length];
        memcpy(&new_number.large_number_array[0],&stream[0],new_number.number_length);
        new_number.cut_zero();
        return p;
    }

    friend ostream & operator<<(ostream &s, LARGE_NUMBERS &l1){
        int i = 0;
        // while(cti(l1.large_number_array[i]) == 0)
        //     i++;
        while(i<l1.number_length){
            s<<l1.large_number_array[i];
            i++;
        }
        return s;
    }
    //Tylko Deklaracje
    LARGE_NUMBERS & resize(int new_size);
    LARGE_NUMBERS & operator=(const LARGE_NUMBERS &tmp);


    LARGE_NUMBERS & operator+(const LARGE_NUMBERS &second){
        static LARGE_NUMBERS tmp;
        if(second.large_number_array == NULL){
            tmp = *this; // żeby kopia była
            return tmp;
        }
        if(this->large_number_array==NULL){
            tmp = second;
            return tmp;
        }
        if(this->number_length>=second.number_length){
            tmp = *this;
            int i = second.number_length-1, j = tmp.number_length-1;
            while(i>=0){
                if(cti(second.large_number_array[i])+cti(tmp.large_number_array[j])>=10){
                    if(j==0){
                        tmp.resize(tmp.number_length+1);
                        j++;
                    }
                    tmp.large_number_array[j] = itc(cti(tmp.large_number_array[j])+(cti(second.large_number_array[i])-10));
                    tmp.large_number_array[j-1] = itc(cti(tmp.large_number_array[j-1]) + 1);
                    for(int k = j-1;cti(tmp.large_number_array[k]) == 10;k--){
                        if(k==0){
                            tmp.resize(tmp.number_length+1);
                            k++;
                            j++;
                        }
                        tmp.large_number_array[k] = itc(cti(tmp.large_number_array[k]) - 10);
                        tmp.large_number_array[k-1] = itc(cti(tmp.large_number_array[k-1]) + 1);

                    }
                }
                else{
                    tmp.large_number_array[j]= itc(cti(tmp.large_number_array[j])+cti(second.large_number_array[i]));
                }
                i--;
                j--;
            }
        }
        else{
            tmp = second;
            int i = this->number_length-1, j = tmp.number_length-1;
            while(i>=0){
                if(cti(this->large_number_array[i])+cti(tmp.large_number_array[j])>=10){
                    // if(j==0){ // nie trzeba bo nie zajdzie przypadek
                    //     tmp.resize(tmp.number_length+1);
                    //     j++;
                    // }
                    tmp.large_number_array[j] = itc(cti(tmp.large_number_array[j])+(cti(this->large_number_array[i])-10));
                    tmp.large_number_array[j-1] = itc(cti(tmp.large_number_array[j-1]) + 1);
                    for(int k = j-1;cti(tmp.large_number_array[k]) == 10;k--){
                        if(k==0){
                            tmp.resize(tmp.number_length+1);
                            k++;
                            j++;
                        }
                        tmp.large_number_array[k] = itc(cti(tmp.large_number_array[k]) - 10);
                        tmp.large_number_array[k-1] = itc(cti(tmp.large_number_array[k-1]) + 1);
                    }
                }
                else{
                    tmp.large_number_array[j] = itc(cti(tmp.large_number_array[j])+cti(this->large_number_array[i]));
                    //cout << tmp.large_number_array[j] << endl;
                }
                i--;
                j--;
            }
        }
        return tmp;
    }
/*

    friend LARGE_NUMBERS operator+(const LARGE_NUMBERS &l1, const LARGE_NUMBERS &l2){
        LARGE_NUMBERS tmp;
        if(l1.large_number_array == NULL){
            tmp = l2;
            return tmp;
        }

        if(l2.large_number_array==NULL){
            tmp = l1;
            return tmp;
        }
        if(l1.number_length>=l2.number_length){
            tmp = l1;
            int i = l2.number_length-1, j = tmp.number_length-1;
            while(i>=0){
                if(cti(l2.large_number_array[i])+cti(tmp.large_number_array[j])>=10){
                    if(j==0){
                        tmp.resize(tmp.number_length+1);
                        j++;
                    }
                    tmp.large_number_array[j] = itc(cti(tmp.large_number_array[j])+(cti(l2.large_number_array[i])-10));
                    tmp.large_number_array[j-1] = itc(cti(tmp.large_number_array[j-1]) + 1);
                    for(int k = j-1;cti(tmp.large_number_array[k]) == 10;k--){
                        if(k==0){
                            tmp.resize(tmp.number_length+1);
                            k++;
                            j++;
                        }
                        tmp.large_number_array[k] = itc(cti(tmp.large_number_array[k]) - 10);
                        tmp.large_number_array[k-1] = itc(cti(tmp.large_number_array[k-1]) + 1);

                    }
                }
                else{
                    tmp.large_number_array[j]= itc(cti(tmp.large_number_array[j])+cti(l2.large_number_array[i]));
                }
                i--;
                j--;
            }
        }
        else{
            tmp = l2;
            int i = l1.number_length-1, j = tmp.number_length-1;
            while(i>=0){
                if(cti(l1.large_number_array[i])+cti(tmp.large_number_array[j])>=10){
                    // if(j==0){ // nie trzeba bo nie zajdzie przypadek
                    //     tmp.resize(tmp.number_length+1);
                    //     j++;
                    // }
                    tmp.large_number_array[j] = itc(cti(tmp.large_number_array[j])+(cti(l1.large_number_array[i])-10));
                    tmp.large_number_array[j-1] = itc(cti(tmp.large_number_array[j-1]) + 1);
                    for(int k = j-1;cti(tmp.large_number_array[k]) == 10;k--){
                        if(k==0){
                            tmp.resize(tmp.number_length+1);
                            k++;
                            j++;
                        }
                        tmp.large_number_array[k] = itc(cti(tmp.large_number_array[k]) - 10);
                        tmp.large_number_array[k-1] = itc(cti(tmp.large_number_array[k-1]) + 1);
                    }
                }
                else{
                    tmp.large_number_array[j] = itc(cti(tmp.large_number_array[j])+cti(l1.large_number_array[i]));
                }
                i--;
                j--;
            }
        }
        return tmp;
    }
*/


};

LARGE_NUMBERS & LARGE_NUMBERS::resize(int new_size){
    //cout << ">>RESIZE<<"<< endl;
    if(new_size == 0){
        large_number_array = new char[1];
        large_number_array[0] = 0;
        return *this;
    }
    if(new_size == number_length)
        return *this;
    LARGE_NUMBERS tmp = *this;
    //this->~LARGE_NUMBERS();
    number_length = new_size;
    large_number_array = new char[new_size];
    if(new_size > tmp.number_length){
        for(int i = 0; i<new_size; i++){
            large_number_array[i] = itc(0);
        }
        memcpy(&large_number_array[new_size-tmp.number_length],&tmp.large_number_array[0],tmp.number_length);
    }
    else{
        memcpy(&large_number_array[0],&tmp.large_number_array[0],new_size);
    }
    return *this;

}
LARGE_NUMBERS & LARGE_NUMBERS::operator=(const LARGE_NUMBERS &tmp){
    if(&tmp==this)
        return *this;
    this->~LARGE_NUMBERS();
    if(tmp.large_number_array == NULL)
        return *this;
    number_length = tmp.number_length;
    large_number_array = new char[number_length];
    memcpy(&large_number_array[0],&tmp.large_number_array[0],number_length);
    return *this;
}

/*
int main() {
    LARGE_NUMBERS pierwsza, druga, trzecia;
    cin>>pierwsza;
    cin >> druga;
    trzecia = pierwsza + druga;
    cout << trzecia << endl;
    return 0;
}*/

#include <stdio.h>
#include <iostream>
#include <math.h>
#include "duzeliczby.cpp"

class LARGE_NUMBERS_2 : public LARGE_NUMBERS{
public:
    LARGE_NUMBERS_2(){
    //    cout << ">>CONSTRUCT 2 <<" << endl;
    }
    ~LARGE_NUMBERS_2(){
    //    cout << ">>DESTRUCT 2<<" << endl;
    }
    LARGE_NUMBERS_2 & operator=(const LARGE_NUMBERS &tmp){
        LARGE_NUMBERS::operator=(tmp);
        return *this;
    }

    LARGE_NUMBERS_2 & operator=(int tmpint){
        this->LARGE_NUMBERS::~LARGE_NUMBERS();
        int tmpintcopy = tmpint;
        large_number_array = new char[10];
        number_length = 0;
        while(tmpintcopy){
            tmpintcopy = floor(tmpintcopy/10);
            number_length++;
        }
        for(int i = number_length - 1;i>=0;i--){
            large_number_array[i] = itc(tmpint%10);
            tmpint = floor(tmpint/10);
        }
        return *this;

    }
    friend bool operator!=(LARGE_NUMBERS_2 &first, LARGE_NUMBERS_2 &second){
        if(first.number_length!=second.number_length)
            return true;
        for(int i = 0; i<first.number_length;i++){
            if(first.large_number_array[i]!=second.large_number_array[i])
                return true;
        }
        return false;
    }
    friend bool operator==(LARGE_NUMBERS_2 &first, LARGE_NUMBERS_2 &second){
        return !(first!=second);
    }
    friend bool operator<(LARGE_NUMBERS_2 &first, LARGE_NUMBERS_2 &second){
        if(first.number_length<second.number_length)
            return true;
        for(int i = 0; i<first.number_length;i++){
            if(first.large_number_array[i]<second.large_number_array[i])
                return true;
            else if(first.large_number_array[i]>second.large_number_array[i])
                return false;
        }
        return false;
    }
    friend bool operator>(LARGE_NUMBERS_2 &first, LARGE_NUMBERS_2 &second){
        if(first.number_length<second.number_length)
            return true;
        for(int i = 0; i<first.number_length;i++){
            if(first.large_number_array[i]>second.large_number_array[i])
                return true;
            else if(first.large_number_array[i]<second.large_number_array[i])
                return false;
        }
        return false;
    }

    LARGE_NUMBERS_2 & increase_by(int at = 0, int increase_number = 1){
        if(at<0){
            resize(number_length+1);
            at++;
        }
        if(cti(large_number_array[at] + increase_number) >= 10){
            if(cti(large_number_array[at]) + increase_number%10 >= 10){
                large_number_array[at] = itc(cti(large_number_array[at]) + increase_number%10 - 10);
                increase_by(at-1,floor(increase_number-increase_number%10)/10+1);
            }
            else{
                increase_by(at,increase_number%10);
                increase_by(at-1,floor(increase_number-increase_number%10)/10);
        }
        }else{
            large_number_array[at] = itc(cti(large_number_array[at]) + increase_number);
        }
        return *this;
    }

    LARGE_NUMBERS_2 & operator*(const LARGE_NUMBERS_2 &second){
        static LARGE_NUMBERS_2 tmp;
        if(second.large_number_array == NULL){
            tmp = *this;
            return tmp;
        }
        if(this->large_number_array==NULL){
            tmp = second;
            return tmp;
        }
        if(this->number_length >= second.number_length){ // Pierwszy dluzszy lub rowny durigemy
            tmp.resize(this->number_length + second.number_length-1);
            for(int i = second.number_length-1;i>=0;i--){
                int count_tmp = tmp.number_length - (second.number_length - i), old_size;
                for(int j = this->number_length-1;j>=0;j--){
                    int tmpint = cti(second.large_number_array[i]) * cti(large_number_array[j]);
                    int old_size = tmp.number_length;
                    tmp.increase_by(count_tmp,tmpint);
                    count_tmp = count_tmp + (tmp.number_length-old_size);
                    count_tmp--;
                }
            }
        }else if(this->number_length < second.number_length){ //drugi dluzszy
            tmp.resize(this->number_length + second.number_length-1);
            for(int i = this->number_length-1;i>=0;i--){
                int count_tmp = tmp.number_length - (this->number_length - i), old_size;
                for(int j = second.number_length-1;j>=0;j--){
                    int tmpint =  cti(large_number_array[i]) * cti(second.large_number_array[j]);
                    //cout << cti(large_number_array[i]) << endl;
                    int old_size = tmp.number_length;
                    tmp.increase_by(count_tmp,tmpint);
                    count_tmp = count_tmp + (tmp.number_length-old_size);
                    count_tmp--;
                }
            }
        }


        return tmp;
    }
};

int main(int argc, char const *argv[]) {
    LARGE_NUMBERS_2 pierwsza, druga, trzecia, czwarta;
    cin >> pierwsza;
    cin >> druga;
    cout << "Dodawanie: ";
    trzecia = pierwsza + druga;
    cout << trzecia << endl;
    cout << "Mnożenie: ";
    trzecia = pierwsza * druga;
    cout << trzecia << endl;

    //
    // int jakisint;
    // cin >> jakisint;
    // czwarta = jakisint;
    // cout << (pierwsza<czwarta) << endl;
    // cout << (pierwsza==druga) << endl;


    // LARGE_NUMBERS_2 a,b,c;
    // cin >> a >> b;
    // c = 11115555;
    // cout << c << endl;
    // c = a+b;
    // c = a+b;
    // cout << c << endl;
    // c = a*b;
    // cout << c << endl;
    // LARGE_NUMBERS_2 d;
    // cin >> d;
    // cout << d << endl;
    // c = c + d;
    // cout << c << endl;
    // cout << (a>b) << " " << (a==b) << " " << (a!=b) << " " << (a<b) << endl;
    return 0;


}

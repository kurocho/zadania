#include <iostream>
#include "listType.cpp"
using namespace std;
template <class T,class K>
class mapa{
public:

	class para{
	public:
		T first;
		K second;
		para *next;

		para(){
			next=NULL;
		}
	};

	para *head;

	mapa(){
		head=NULL;

	}
	~mapa(){
		para *current;
		while(head!=NULL){
			current = head->next;
			delete head;
			head = current;
		}
	}


	K &operator[](const T &value){
		if (head==NULL){
			head = new para;
			head->first=value;
			return head->second;

		}
		para *current = head;
		while(current!=NULL){
			if(current->first==value){
				return current->second;
			}
			if(current->next==NULL){
				break;
			}
			current=current->next;
		}
		para *temp = new para;
		current->next=temp;
		temp->first=value;
		return temp->second;

	}
	mapa &operator=(const mapa &object){
		if(this==&object){
			return *this;
		}
		this->~mapa();
		if(object.head==NULL)
			return *this;

		para *temp = new para;
		para *current = object.head;
		temp->first=current->first;
		temp->second=current->second;
		this->head=temp;

		while(current->next!=NULL){
			para *temp2 = new para;
			temp->next = temp2;
			current=current->next;
			temp2->first=current->first;
			temp2->second=current->second;
			temp = temp2;

		}
		return *this;
	}

	class iterator{
	public:
		para *current;

		iterator(para &object){
			current=&object;
		}

		iterator &operator=(const iterator &temp){
			current = temp.current;
			return *this;
		}
		bool operator!=(const iterator &temp){
			if(current!=temp.current)
				return true;
			return false;
		}
		iterator operator++(int something){
			current=current->next;
			return *this;
		}
		iterator operator++(){
			iterator temp = *this;
			current = current->next;
			return temp;
		}
		para *operator->(){
			return current;
		}

	};

	iterator begin(){
		return *head;
	}
	iterator end(){
		para *current = head;
		while(current!=NULL)
			current = current->next;
		return *current;
	}


};

int main(){
	mapa<string,string> m1,m2,m3;

   	m1["monkey"]="malpa";
	m1["horse"]="kon";
	m2["jablko"]="apple";
	string s;
	s=m1["monkey"];
	m3=m1;
	m3=m1;
	cout<<s<<" "<<m2["jablko"]<<endl;
	for(mapa<string,string>::iterator i=m3.begin();i!=m3.end();i++)
		cout<<i->first<<" "<<i->second<<endl;
	for(mapa<string,string>::iterator i=m3.begin();i!=m3.end();i++){
		if(i->second=="kon")
			cout<<i->first<<endl;
	}
    return 0;

}

#include <iostream>

using namespace std;
//***************************************
template <class Type> // zeby klasa dzialala na roznych typach: dobule, int, etc.
class COMPLEX{
    public:
    Type Re,Im; // wszedzie trzeba dawac nazwe wczesniej zdefiniowana
    COMPLEX(Type _Re, Type _Im){
        Re = _Re;
        Im = _Im;
    }
    COMPLEX<Type> & operator=(COMPLEX<Type> & p1){ // trzeba uzywac <Type>
        Re = p1.Re;
        Im = p1.Im;
        return *this;
    }

    COMPLEX<Type> & operator+(COMPLEX<Type> & p1){
        static COMPLEX<Type> temp;
        temp.Re = Re + p1.Re;
        temp.Im = Im + p1.Im;
        return temp;
    }
    friend istream & operator>>(istream &s, COMPLEX<Type> &o1){
        //o1.~COMPLEX();
        s >> o1.Re;
        s >> o1.Im;
    }
    friend ostream & operator<<(ostream &s, COMPLEX<Type> &o1){
        s << "\nReal:\n"<< o1.Re;
        s << "\nImaginary:\n" << o1.Im << endl;

        return s;
    }

};

//***************************************

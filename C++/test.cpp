#include <iostream>
#include <stdio.h>

using namespace std;
class k1{
private:
    int a;
protected:
    int b;
public:
    int c;

    k1(){
        cout << "Konstruktor k1" << endl;
        a = 0;
        b = 1;
        c = 2;
    }
    ~k1(){
        cout << "Destruktor k1" << endl;
    }

    k1 & operator=(k1 & second){
        this->a = second.a;
        this->b = second.b;
        this->c = second.c;
        return *this;
    }

};

class k2:public k1{
public:
    k2(){
        cout << "Konstruktor k2" << endl;
        b = 3;
        c = 4;
    }
    ~k2(){
        cout << "Destruktor k2" << endl;
    }
    k2 & operator=(k2 & second){
        //a = second.a; nie mozna bo private
        b = second.b;
        c = second.c;
        return *this;
    }
};
int main(int argc, char const *argv[]) {
    // k1 pierwszy,drugi;
    k2 trzeci,czwarty;
    // pierwszy.c = 0;
    // cout << pierwszy.c << endl;
    // pierwszy = drugi;
    // cout << pierwszy.c << endl;
    trzeci.c = 0;
    cout << trzeci.c << endl;
    trzeci = czwarty;
    cout << trzeci.c << endl;

}

#include <iostream>

using namespace std;
//***************************************
class MACIERZ {
    int *wsk;
    int rozmiar;
public:
    MACIERZ(){
        wsk = NULL;
        rozmiar = 0;
    }

    MACIERZ(MACIERZ & o1){
        //cout << "Konstruktor kopiujący MACIERZY" << endl;
        rozmiar = o1.rozmiar;
        wsk = new int[rozmiar*rozmiar];
        for(int i=0;i<rozmiar*rozmiar;i++)
            wsk[i] = o1.wsk[i];
    }

    ~MACIERZ(){
        cout << "Destruktor MACIERZY" << endl;
        if(wsk != NULL){
            delete []wsk;
            wsk = NULL;
            rozmiar = 0;
        }
    }
    friend istream & operator>>(istream &s, MACIERZ &o1){
        o1.~MACIERZ();
        cout << "Rozmiar MACIERZY: " << endl;
        s >> o1.rozmiar;
        cout << "Elementy: " << endl;
        o1.wsk = new int[o1.rozmiar*o1.rozmiar];
        for(int i=0;i<o1.rozmiar*o1.rozmiar;i++)
                s >> (o1.wsk[i]);
        return s;
    }
    friend ostream & operator<<(ostream &s, MACIERZ &o1){
        for(int i=0;i<o1.rozmiar*o1.rozmiar;i++){
            s << o1.wsk[i] << " ";
            if((i+1)%o1.rozmiar==0)
                s << "\n";
        }
        return s;
    }

    MACIERZ & operator=(const MACIERZ & o1){
        cout << "Operator przypisania MACIERZY" << endl;
        if(this == &o1)
            return *this;
        this->~MACIERZ();
        rozmiar = o1.rozmiar;
        wsk = new int[rozmiar*rozmiar];
        for(int i=0;i<rozmiar*rozmiar;i++)
            wsk[i] = o1.wsk[i];
        return *this;
    }


    MACIERZ operator+(MACIERZ & o1){
        cout << "Dodawanie MACIERZY" << endl;
        MACIERZ temp;
        temp.rozmiar = this->rozmiar;
        temp.wsk = new int[rozmiar*rozmiar];
        for(int i = 0; i<rozmiar*rozmiar;i++){
            temp.wsk[i] = this->wsk[i] + o1.wsk[i];
        }
        return temp;
    }

    // friend MACIERZ operator+(MACIERZ & a, MACIERZ & b){
    //     cout << "Dodawanie zewn" << endl;
    //     MACIERZ temp;
    //     temp.rozmiar = a.rozmiar;
    //     temp.wsk = new int[temp.rozmiar*temp.rozmiar];
    //     for(int i = 0; i<temp.rozmiar*temp.rozmiar;i++){
    //         temp.wsk[i] = a.wsk[i] + b.wsk[i];
    //     }
    //     return temp;
    // }

    MACIERZ & operator*(MACIERZ & o1){
        static MACIERZ temp;
        cout << "Mnożenie MACIERZY" << endl;
        int iloczyn=0, wiersz=0, kolumna=0;
        if(this->rozmiar!=o1.rozmiar)
        {
            if(temp.wsk!=NULL)
            {
                    temp.~MACIERZ();
            }
            temp.rozmiar=0;
            return temp;
        }
        else
        {
            if(temp.wsk!=NULL)
            {
                temp.~MACIERZ();
            }
            temp.rozmiar=this->rozmiar;
            temp.wsk=new int[temp.rozmiar*temp.rozmiar];
            for(int i=0, r=temp.rozmiar; i<(temp.rozmiar*temp.rozmiar); i++)
            {
                wiersz=i/r;
                kolumna=i%r;
                for(int j=0;j<r;j++)
                {
                    iloczyn=iloczyn+(this->wsk[wiersz*r+j]*o1.wsk[kolumna+j*r]);
                }

                temp.wsk[i]=iloczyn;
                iloczyn=0;
            }
            return temp;
        }
    }
    bool operator<(MACIERZ & o1){
        cout << "Operator mniejszosci MACIERZY" << endl;
        int sum1 = 0;
        int sum2 = 0;
        for(int i = 0; i<rozmiar*rozmiar; i++)
        {
            sum1+=wsk[i];
            sum2+=o1.wsk[i];
        }
        if(sum1<sum2)
            return true;
        else
            return false;
    }
    bool operator==(MACIERZ & o1){
        cout << "Operator rownosci MACIERZY" << endl;
        if(rozmiar<o1.rozmiar)
            return true;
        else
            return false;

        for(int i = 0; i<rozmiar*rozmiar; i++)
        {
            if(wsk[i]!=o1.wsk[i])
                return false;
        }
        return true;
    }
    bool operator!=(MACIERZ & o1){
        return !(*this==o1);
    }
};

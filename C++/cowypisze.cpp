#include <cstdio>

class A {
    int i;
public:
    A(int _i = 0) : i(_i){
        printf("A%d\n",i);
    }
    ~A(){ // nie wirtualna to tylko A a jezeli writualna to B i potem A
        printf("~A%d\n",i);
    }
};

class B : public A{
    int x;
    A a; // A0
public:
    B(int _x ):A(1),x(_x){}; //wczesniej od tego wyzej A1 gdy niewirtualny destruktor w A
    ~B(){
        printf("~B %d\n",x);
    }
};

B b(5);

int main(){
    A*ptr = new B(3);
    delete ptr;
    return 0;
}

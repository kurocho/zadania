# Create data
x <- seq(-10, 10, by=0.01)

# Probability density function:
f1 <- dnorm(x, mean=0, sd=1)
f2 <- dnorm(x, mean=-3, sd=1)
f3 <- dnorm(x, mean=0, sd=3)
f4 <- dnorm(x, mean=3, sd=0.7)

# Cummulative density function:
F1 <- pnorm(x, mean=0, sd=1)
F2 <- pnorm(x, mean=-3, sd=1)
F3 <- pnorm(x, mean=0, sd=3)
F4 <- pnorm(x, mean=3, sd=0.7)

# Plot parameters:
linetype <- rep("l",4)
#linecol <- c("grey0", "grey40", "gray60", "grey0")
linecol <- c("red2", "royalblue", "orange", "forestgreen")
linetype <- c(1, 2, 3, 1)
linewidth <- c(1,2,2,2)
linenames <- c(expression(f[1](x)),expression(f[2](x)),expression(f[3](x)),expression(f[4](x)))

# Plot f:
dat <-cbind(f1, f2, f3, f4)
matplot(x, dat, type ="l", col=linecol, lwd=linewidth, lty = linetype, xlab="x", ylab="f(x)") #plot
legend("topright", linenames, cex=0.8, lty=linetype, lwd=linewidth,col=linecol) 

# Plot F:
datF <-cbind(F1, F2, F3, F4)
#matplot(x, datF, type ="l", col="gray40", lwd=2, lty = 1, xlab="x", ylab="F(x)") #plot
matplot(x, datF, type ="l", col=linecol, lwd=linewidth, lty = linetype, xlab="x", ylab="F(x)") #plot
legend("topright", linenames, cex=0.8, lty=linetype, lwd=linewidth,col=linecol) 

x <- seq(-4,4, by=0.01)
f <- dnorm(x)
title = "Funkcja gestosci prawdopodobienstwa dla N(0,1)"
plot(x, f, type="l", xlab="x", ylab="f(x)", main=title)

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdbool.h>

struct word {
   char content[50];
   int frequency;
   struct word *next;
};

struct WordList
{
	struct word *head;
	int size;
};

void ReadDictionary(struct WordList *pList, const char *filename)
{
	pList->size = 0;
	FILE *file = fopen(filename, "r");
	
	if(!file) 
	{
        	fputs("File error.\n", stderr);
    	}

	if (file) 
	{	char str[50];
	
	    while (fscanf(file, "%49s", str)!=EOF)
	    {
			struct word *link = (struct word*) malloc(sizeof(struct word));
			strncpy(link->content, str, 50);
			
			if (pList -> head)
			{  	
				struct word *ptr;
	   			ptr = pList -> head -> next;
	   			bool add = true;
	   			while(ptr != NULL)
	   			{
	   				if(strcmp(ptr->content,link->content)==0)
	   				{
	   					ptr->frequency += 1;
	   					add = false;
					}
					ptr = ptr->next;
				}
				if (add)
				{
					link->next = pList -> head; 
					pList -> head = link;
     					pList -> size += 1; 
					link->frequency = 1;
				}
			}
			else
			{
				pList -> head = link;
     			pList -> size += 1; 
			}
     		
     		
		}
    fclose(file);
	}
	
}

void printList(struct WordList *pList) 
{
   struct word *ptr;
   ptr = pList -> head;
   
   while(ptr != NULL) 
    {
      printf("%s %d\n",ptr->content,ptr->frequency);
      ptr = ptr->next;
    }
}

void sortList(struct WordList *pList)
{
	char temp[50];
	struct word *ptr;
	ptr = pList -> head;
	while(ptr != NULL)
    {
        struct word *ptr2;
		ptr2 = pList -> head;
		while(ptr2 != NULL)
        {
            if (strcmp(ptr->content,ptr2->content) < 0)
            {
                strcpy(temp, ptr2->content);
                strcpy(ptr2->content,ptr->content);
                strcpy(ptr->content, temp);
            }
            ptr2 = ptr2->next;
        }
        ptr = ptr->next;
    }
}

int countWords(struct WordList *pList)
{
	int number = 0;
	struct word *ptr;
	ptr = pList -> head;
	while(ptr != NULL)
	{	
		number += ptr->frequency;
		ptr = ptr->next;
	}
return number;
}

char * FindLongestOne(struct WordList *pList)
{
	struct word *ptr;
	ptr = pList -> head;
	char longest[50];
	strcpy(longest,ptr->content);
	while(ptr->next != NULL)
	{	
		if( strlen(longest) < strlen(ptr->next->content) )
		{
			strcpy(longest, ptr->next->content);
		}
		ptr = ptr->next;
	}
	printf("The longest one: %s \n",longest);
}

int main(int argc, char **argv)
{
	struct WordList List;
        List.head = NULL;

	ReadDictionary(&List,argv[1]);
	sortList(&List);
	printList(&List);
	printf("%d words of %d types\n",countWords(&List),List.size);
	FindLongestOne(&List);
	
return 0;
} 

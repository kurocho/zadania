#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>

typedef struct kolo
{
    char *id;
    double x, y, promien;
    struct kolo *kolejne;
}Kolo;

typedef struct lista
{
    Kolo *pierwsze;
    int rozmiar;
}Lista;

void inicjujliste(Lista *lista)
{
    lista->pierwsze=0;
    lista->rozmiar=0;
}

void wpiszjednokolo(Lista *lista)
{
    double x, y, promien;
    Kolo *nowe, *tymczasowy;
    char bufor[30];
    nowe=(Kolo*)malloc(sizeof(Kolo));
    printf("Podaj identyfikator (nazwe) kola:\n");
    scanf("%s",bufor);
    nowe->id = (char*)malloc(sizeof(char)*(strlen(bufor)+1));
    strcpy(nowe->id,bufor);
    printf("\nPodaj kolejno (oddzielajac enterami): x, y srodka, a nastepnie promien kola:\n");
    scanf("%lf", &nowe->x);
    scanf("%lf", &nowe->y);
    scanf("%lf", &nowe->promien);
    tymczasowy=lista->pierwsze;
    if(tymczasowy)
    {
        while(tymczasowy->kolejne)
        {
            tymczasowy=tymczasowy->kolejne;
        }
        tymczasowy->kolejne=nowe;
        lista->rozmiar+=1;
    }
    else lista->pierwsze=nowe;
}

void wpisujkola(Lista *lista)
{
    int pseudobool;
    pseudobool=1;
    while(pseudobool)
    {
        wpiszjednokolo(lista);
        printf("Czy chcesz wprowadzic kolejne kolo? Wpisz 1 jesli tak, 0 jesli nie: ");
        scanf("%d", &pseudobool);
    }
}

void wypisujkola(Lista *lista){
    Kolo *n;
    for(n = lista->pierwsze;n;n=n->kolejne){
        printf("Nazwa: %s  x: %lf  y: %lf  promien: %lf\n",n->id,n->x,n->y,n->promien);
    }
}

int main(void)
{
    Lista *lista;
    inicjujliste(lista);
    wpisujkola(lista);
    wypisujkola(lista);
    return 0;
}

//************* Program przydzielajacy sedziow **********************/
#include<stdio.h>
#include<string.h>
#include<stdlib.h>
#include <time.h>
/************************ Struktury *********************************/
typedef struct node {
	char *name;
	int id;
	struct node *next;
}Node;

typedef struct LIST {
	Node *head;
	int size;
}List;

/************************ Funkcje *********************************/
void init(List*list)
{
       list->head=0;
	   list->size=1;
}


void create_referee(List *list){
	char bufor[30];
	Node *nowy, *pom = 0;
	srand(time(NULL));
	nowy = (Node*)malloc(sizeof(Node));
	printf("Dodaj nazwisko sedziego: ");
	scanf("%s", bufor);
	nowy->id = rand()%1000;
	nowy->name=(char*)malloc(sizeof(char)*(strlen(bufor)+1));
	strcpy(nowy->name,bufor);
	nowy->next = 0;
	pom=list->head;
	if(pom){
		while(pom->next){
			pom=pom->next;
		}
		pom->next = nowy;
		list->size +=1;
	}
	else
		list->head = nowy;
}


void print_referee(List *list){
	Node *n;
	for(n = list->head;n;n=n->next){
		printf("Nazwisko: %s,  Identyfikator: %d\n",n->name, n->id);
	}

}

void assign_referee(List *list, int ilosc_meczy){
	int i, j, where, idsedziego;
	Node *pom;
	for(i=0;i<ilosc_meczy;i++){
		srand(time(NULL)*i);
		where = rand() % list->size;
		pom = list->head;
		for(j=0;j<where;j++){
			pom = pom->next;
		}
		idsedziego = pom->id;
		printf("Id sedziego: %d mecz numer: %d\n",idsedziego,i+1);


	}

}
int number_of_matches(int nd){
	int total;
	total=nd*(nd-1)/2;
	return total;
}
/***********************************************************************/
int main(){
	List *lista = 0;
	int *druzyny, liczba_druzyn, mecze;
	printf("Podaj liczbe druzyn: ");
	scanf("%d",&liczba_druzyn);
	druzyny = (int*) malloc((liczba_druzyn+1)*sizeof(int));
	init(lista);
	create_referee(lista);
	create_referee(lista);
	create_referee(lista);
	print_referee(lista);
	mecze = number_of_matches(liczba_druzyn);
	assign_referee(lista,mecze);
	return 0;
}


#include <stdio.h>
#include <stdlib.h>

struct notZeroPart
{
	double value;
	int pos[2];
	struct notZeroPart * next;	
};

struct Matrix
{
	struct notZeroPart *head;
	int rows;
	int columns;
};

struct MinMaxValuePointers
{
	struct notZeroPart *min;
	struct notZeroPart *max;
};

void RewriteMatrix( double *M, int rows, int columns, struct Matrix *MX )
{	
    MX->head = NULL;
    MX->rows = rows;
    MX->columns = columns;
	int i, j;
    for (i = 0; i < rows; i++)
      for (j = 0; j < columns; j++)
      {
      	if (*((M+i*rows) + j) != 0 )
      	{
      		struct notZeroPart *link = (struct notZeroPart*) malloc(sizeof(struct notZeroPart));
      		link -> value = *((M+i*rows) + j);
      		link -> pos[0]= i;
      		link -> pos[1]= j;
      		if ( MX -> head )
      		{
				link -> next = MX -> head; 
				MX -> head = link;  
		    }
		    else
		    {
		    	MX -> head = link;
				MX -> head -> next = NULL; 
			}
      	}
	  }
}

void PrintMatrix(struct Matrix *MX)
{
	double PrintMatrix[MX->rows][MX->columns];
	int i, j;
    for (i = 0; i < MX->rows; i++)
      for (j = 0; j < MX->columns; j++)
      	PrintMatrix[i][j]=0;

    struct notZeroPart *ptr;
		ptr = MX -> head;
		while(ptr != NULL)
		{
			PrintMatrix[ptr->pos[0]][ptr->pos[1]] = ptr->value;
			ptr=ptr->next;
		}	
	printf("\n");
	for (i = 0; i < MX->rows; i++)
	{
	  printf("\n");
      for (j = 0; j < MX->columns; j++)
      	printf("%f ",PrintMatrix[i][j]);
	}
}

void MinMaxValue(struct Matrix *MX, struct MinMaxValuePointers *Result)
{
	struct notZeroPart *ptr;
	ptr = MX -> head;
	Result -> min = MX -> head;
	Result -> max = MX -> head;
	while(ptr != NULL)
	{
		if( ptr->value > Result->max->value )
		{
			Result->max = ptr;
		}
		if( ptr->value < Result->min->value )
		{
			Result->min = ptr;
		}
		ptr = ptr -> next;
	}
}

int main ()
{
	double A[3][3]={{1,0,3},
					{0,3,0},
					{4,0,0}};
					
	double C[4][4]={{2,0,1,0},
					{0,3,0,1},
					{0,5,0,0},
					{4,0,0,7}};
					
	double B[5][7]={{2,0,0,4,0,1,0},
					{0,3,0,10,2,0,8},
					{0,3,-7,0,2,0,19},
					{0,5,6,69,0,0,0},
					{4,0,0,0,0,0,7}};
					
	struct Matrix MX;				
	RewriteMatrix( (double*)B, (sizeof(B)/sizeof(B[0])), (sizeof(B[0])/sizeof(B[0][0])), &MX );
	PrintMatrix( &MX );
	struct MinMaxValuePointers Result;
	MinMaxValue(&MX, &Result);
	printf("\n\nMax: %f",Result.max->value);
	printf("\npos: (%d,%d)",Result.max->pos[0]+1,Result.max->pos[1]+1);
	printf("\n\nMin: %f",Result.min->value);
	printf("\npos: (%d,%d)",Result.min->pos[0]+1,Result.min->pos[1]+1);
	return 0;
}

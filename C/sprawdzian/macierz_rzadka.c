#include <stdio.h>
#include <stdlib.h>

//int matrix[3][2]={{9,2},{7,5},{8,9}};
//int number_of_columns, int number_of_lines;
struct column{
	char line[10];
};


void* create_struct_columns(int number_of_columns, int number_of_lines, int matrix[][2]);



int main(){
	int k,l;
	int highest=0;
	int new_matrix[3][2] ={{1,2},{3,4},{5,6}};
	int number_of_columns, number_of_lines;
	struct column *clmn;
	number_of_columns=(sizeof(new_matrix[0]))/(sizeof(new_matrix[0][0]));
	number_of_lines=(sizeof(new_matrix))/(sizeof(new_matrix[0]));
	
	clmn = (struct column*) create_struct_columns(number_of_columns, number_of_lines, new_matrix);
	for (k=0;k<number_of_lines;k++){
		for (l=0;l<number_of_columns;l++){
			if (clmn[l].line[k]!=0){
				
				printf("%d",clmn[l].line[k]);
				if (clmn[l].line[k]>highest){
					highest=clmn[l].line[k];
				}
			}
			
		}
		printf("\n");
	}
	printf("\n%d",highest);	
}
void* create_struct_columns(int number_of_columns, int number_of_lines, int matrix[][2]){
	struct column *clmn= (struct column*) malloc(number_of_columns * sizeof(struct column));
	
	int i,j;
	
	for(i=0;i<number_of_columns;i++){
		
		for (j=0;j<number_of_lines;j++){
			
			clmn[i].line[j]=matrix[j][i];
				
		}	
	}
	return clmn;
}

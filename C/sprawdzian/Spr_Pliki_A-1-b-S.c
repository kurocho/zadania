#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/*
LISTA-PLIKI
wersja: A-2-b-S
*/
typedef struct NODE Node;
typedef struct LIST List;
struct NODE
{
	char *slowo;
	int krotnosc;
	struct NODE *next;
};

struct LIST
{
	Node *head;
};

void init(List *list)
{
       list->head=0;
}

void isCorrect(char *bufor)
{
	int lel, first, last, i;
	lel=strlen(bufor)-1;
	first=bufor[0];
	if (!(first<91 && first>64) && !(first<123 && first>96))
	{
		for(i=1;i<=lel;i++){
			bufor[i-1] = bufor[i];
		}
		bufor[lel]=bufor[lel+1];
	}
	lel=strlen(bufor)-1;
	last=bufor[lel];
	if (!(last<91 && last>64) && !(last<123 && last>96))
	{
		bufor[lel]=bufor[lel+1];
	}
}

void readFile(List *list)
{
	FILE *plik=NULL;
	char bufor[50];
	Node *nowy, *pom;
	plik =fopen("tekst.txt","r");
	if (plik == NULL)
	{
		printf("nie udalo sie otworzyc pliku tekst.txt");
	}
	else
	{
		while(fscanf(plik,"%s",bufor) != EOF)
		{
			//isCorrect(bufor);
			/*usuwanie "doklejonych" znaków specjalnych
			(usuwa tylko jeden, nie usuwa jak sa 3 kropki)*/
			nowy = (Node*)malloc(sizeof(Node));
			nowy->next=0;
			nowy->krotnosc = 1;
			nowy->slowo=(char*)malloc(sizeof(char)*(strlen(bufor)+1));
			strcpy(nowy->slowo,bufor);
			//pom=list->head;
			// if (pom == 0)
			// 	list->head = nowy;
			// else
			// {
			// 	for(pom = list->head;pom->next;pom = pom->next){
			// 		if(strcmp(pom->slowo, nowy->slowo)==0){
			// 			//wyraz sie powtarza
			// 		//	printf("Nowy: %s\n",nowy->wyraz.slowo);
			// 			pom->krotnosc += 1;
			// 			break;
			// 		}
			// 		else if (strcmp(pom->slowo, nowy->slowo)>0)
			// 		{
			// 			//dodawanie zgodnie z alfabetyczna kolejnoscia
			// 			nowy->next = pom->next;
			// 			pom->next = nowy;
			// 			break;
			// 		}
			// 		else if(pom->next == 0){
			// 			//dodawanie na koncu
			// 			pom->next = nowy;
			// 		}
			// 	}
			//
			// }
		}
	}
	fclose(plik);
}

void PrintList(List *list)
{
	Node *i;
    for(i=list->head;i;i=i->next){
		printf("%d x %s \n",i->krotnosc,i->slowo);
	}
}

void ileWyrazow(List *list)
{
	Node *i;
	int suma=0;

	for(i=list->head;i;i=i->next){
		suma+=i->krotnosc;
		i = i->next;
	}
	printf("\n %d wyrazow w pliku.\n", suma);
}

void ktoryNajwiekszy(List *list)
{
	Node *i;
	char buff[30];
	int rozmiar=0;


	for(i = list->head;i;i=i->next){
		if (strlen(i->slowo)>rozmiar)
		{
			rozmiar = strlen(i->slowo);
			strcpy(buff,i->slowo);
		}

	}
	printf("%s to najdluzsze slowo\n",buff );
}

//_______________________________________________________MAIN

int main(int argc, char const *argv[])
{
	List *chyba_listy;
	readFile(chyba_listy);
	//PrintList(chyba_listy);			//to można pominąć
	// ileWyrazow(chyba_listy);
	// ktoryNajwiekszy(chyba_listy);
	return 0;
}

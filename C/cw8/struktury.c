#include <stdlib.h>  /* malloc */
#include <stdio.h>   /* printf */
struct Sam {
    double predk;
    int    roczn;
    struct bak;
} skoda, fiat = {100, 1998};

void pr(const char*,const struct Sam*);
  
int main(void) {
    struct Sam toyota, *mojsam = &toyota, *vw;
    struct Sam *mojaskoda = &skoda;
    printf("\nObiekty \'Sam\' maja rozmiar %li bajtow\n\n",
    sizeof(struct Sam));
    mojaskoda->predk =  120;
    mojaskoda->roczn = 1995;
      
    mojsam->predk =  180; /* (*mojsam).predk = 180 */
    toyota.roczn  = 2000;

    vw = (struct Sam *) malloc(sizeof(struct Sam));
    vw->predk =  175;
    vw->roczn = 2003;
     
    pr("Skoda ",mojaskoda); /* wskaznik bez & */
    pr("Fiat  ",&fiat); /* zwykla zmienna strukturalna z & */
    pr("Toyota",&toyota);
    pr("mojsam", mojsam);
    pr("VW    ", vw);
      
    free(vw);
      
    return 0;
}
  
void pr(const char *nazwa, const struct Sam *sam) {
    printf("%s: predkosc %3.1f, rocznik %4d\n",
    nazwa, sam->predk, sam->roczn);
}

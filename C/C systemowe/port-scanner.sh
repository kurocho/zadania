#!/bin/bash

###
# Skaner portów (sprawdza które porty są otwarte).
# Jako argument wywołania podaj adres serwera, który chcesz przeskanować,
# np. ./port-scanner.sh localhost
###

host=$1
port_first=1
port_last=65535
for ((port=$port_first; port<=$port_last; port++))
do
  # echo "Skanowanie portu $port..."
  timeout 1 bash -c "(echo >/dev/tcp/$host/$port) >/dev/null 2>&1" && echo "$port otwarty!"
done

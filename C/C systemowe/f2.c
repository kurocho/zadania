#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/stat.h>

#define BUFSIZE 1024

int main (int argc, char **argv) {
    int f1, f2, c, ilewczytano, ilezapisano;
    char b[BUFSIZE], *n1, *n2;
	mode_t tryb;
	if(argv[3])
		tryb=O_RDWR;
	else
		tryb=O_RDONLY;


	c=10;

    n1 = argv[1];
    n2 = argv[2];
	if(argc>1){
	    f1 = open (n1,tryb);
	    f2 = open (n2, O_WRONLY | O_CREAT | O_TRUNC, 0600);
		while(ilewczytano = read(f1, b, c)){
	    	printf("%s: Przeczytano %d znaków z pliku %s: \"%s\"\n",
		   		argv[0], ilewczytano, n1, b);


	    ilezapisano = write (f2, b, c);
	    printf("%s: Zapisano %d znaków do pliku %s: \"%s\"\n",
		   argv[0], ilezapisano, n2, b);
	}

	    close(f1);
	    close(f2);
	}
	else
		printf("Nie podano pliku jako argument!\n");

    return(0);
}

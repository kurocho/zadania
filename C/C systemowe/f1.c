#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>

#define BUFSIZE 1024

int main (int argc, char **argv) {
    int f1, c, ileznakow;

    char b[BUFSIZE], *n1;

    c = 10;
    n1 = argv[1];
    if(argc!=1){ /*1 zadanie 1 kropka */
	    f1 = open (n1, O_RDONLY);
		ileznakow = read (f1, b, c); /*1 zadanie 2 kropka */
	    printf("%s: Przeczytano %d znaków z pliku %s: \"%s\"\n",
		   argv[0], ileznakow, n1, b);
	    close(f1);
	}
	else
		printf("Nie podano pliku jako argument!\n");

    return(0);
}

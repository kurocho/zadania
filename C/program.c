/************** Program paczki  *********************/
#include<stdio.h>
#include<stdlib.h>
/******************* Struktury ***********************/
typedef struct paczka {
    int id;
    double masa;
    struct paczka *next;
}Paczka;

typedef struct LIST {
    Paczka *head;
    int rozmiar;
}List;

typedef struct parypaczek {
    int idpierwszej;
    int iddrugiej;
    double masaobu;
}Parypaczek;




/****************** Funkcje **************************/
void init(List *list){
       list->head=0;
       list->rozmiar=0;
}

void wpiszpaczke(List *list){
    Paczka *nowa, *pom;
    nowa = (Paczka*)malloc(sizeof(Paczka));
    printf("Podaj numer paczki: ");
    scanf("%d", &nowa->id);
    printf("Podaj mase paczki: ");
    scanf("%lf", &nowa->masa);
    nowa->next = 0;
    pom = list->head;
    if(pom){
        while(pom->next){
            pom=pom->next;
        }
        pom->next = nowa;
        list->rozmiar += 1;
    }
    else
        list->head = nowa;

}

void masapary(Parypaczek *masyparpaczek, int rozmiar){
    int i, numer1, numer2;
    printf("Podaj id pierwszej paczki: ");
    scanf("%d",&numer1);
    printf("Podaj id drugiej paczki: ");
    scanf("%d",&numer2);
    for(i = 0;i<rozmiar;i++)
    {
        if((masyparpaczek[i].idpierwszej == numer1 && masyparpaczek[i].iddrugiej == numer2) || (masyparpaczek[i].idpierwszej == numer2 && masyparpaczek[i].iddrugiej == numer1))
            printf("Masa pary ktorej podales to: %lf",masyparpaczek[i].masaobu);
    }

}

void wpisujpaczki(List *list){
    int test;
    test=1;
    while(test)
    {
        wpiszpaczke(list);
        wpiszpaczke(list);
        printf("Wpisz 1 jesli chcesz wprowadzc kolejne paczki, 0 jesli nie: ");
        scanf("%d", &test);
    }
}

void liczmasyparpaczek(List *list, Parypaczek *masyparpaczek){
    int licznikpar;
    Paczka *pom, *pom2;
    pom = list->head;
    licznikpar = 0;
    while(pom){
        pom2 = pom->next;
        while(pom2){
            masyparpaczek[licznikpar].idpierwszej = pom->id;
            masyparpaczek[licznikpar].iddrugiej = pom2->id;
            masyparpaczek[licznikpar].masaobu = pom->masa;
            masyparpaczek[licznikpar].masaobu += pom2->masa;
            printf("Masa paczki dla id1: %d i id2: %d rowna sie %lf\n", pom->id,pom2->id ,masyparpaczek[licznikpar].masaobu);
            licznikpar+=1;;
            pom2 = pom2->next;
            }
        pom = pom->next;
    }
}
void printlist(List *list){
    Paczka *pom;
    pom = list->head;
    while(pom){
        printf("Paczka o id: %d ma mase: %lf\n",pom->id, pom->masa);
        pom = pom->next;
    }
}
/**************************************************/
int main(int argc, char const *argv[]){
    List paczki;
    Parypaczek *masyparpaczek;
    int iloscparpaczek = 1, i;
    init(&paczki);
    wpisujpaczki(&paczki);
    for(i=1;i<=paczki.rozmiar;i++){
        iloscparpaczek*=i; /* silnia ilosci paczek */
    }
    masyparpaczek = (Parypaczek*)malloc(sizeof(Parypaczek)*iloscparpaczek);
    printlist(&paczki);
    liczmasyparpaczek(&paczki,masyparpaczek);
    masapary(masyparpaczek, iloscparpaczek);
    return 0;
}

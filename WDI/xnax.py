#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Oct 19 13:16:45 2016

@author: piotr
"""
#Z x na 10
typlosowy = int(input("Podaj z jakiego systemu mam przeliczyć liczbe ( max 36 ze względu na ilosc liter w alfabecie ): "))
losowy = input("Podaj liczbę w systemie: typlosowy: ") #czytam string
dlugosclosowy = len(losowy)
potega = 0
dziesiatkowa = 0
losowyint = []
for i in range(0,dlugosclosowy):
    if(ord(losowy[i])>=97 and ord(losowy[i])<=122):
        losowyint.append(ord(losowy[i])-87)
    else:
        losowyint.append(int(losowy[i]))

while(dlugosclosowy):
    dziesiatkowa += (int(losowyint[dlugosclosowy-1])*(typlosowy**potega))
    potega += 1
    dlugosclosowy -= 1

del losowyint


#z 10 na x
jakityp = int(input("Podaj na jaki system chcesz przeliczyc ( max 36 ze względu na ilosc liter w alfabecie ): "))
inna = []
dlugoscinnej = 0
while(dziesiatkowa):
    inna.insert(0, dziesiatkowa%jakityp)
    dziesiatkowa/=jakityp
    dziesiatkowa = int(dziesiatkowa)
    dlugoscinnej+=1

#zamiana tablicy w jeden ładny string
innastr = ''
for i in range(0,dlugoscinnej):
    if(inna[i]>9):
        innastr += chr(inna[i]+87) # liczby powyżej 9 zamiana na małe litery alfabetu
    else:
        innastr += str(inna[i])

del inna

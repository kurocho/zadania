# -*- coding: utf-8 -*-
"""
Created on Mon Dec 12 11:28:03 2016

@author: snoppiot
"""
class Node:

    def __init__(self, name, score):
        self.name = name
        self.score = score
        self.next = None
        self.prev = None

    def __str__(self):
        return str(self.name,self.score)

class UnidirectionalList:

    def __init__(self):
        self.head = None
        self.tail = None

    def addLast(self, name, score):
        if not self.head:
            n = Node(name,score)
            self.head = n
            self.tail = n
        else:
            n = self.head
            while n.next != None:       # przechodzi na koniec
                n = n.next
            new_node = Node(name,score) # tworzy nowego node
            n.next = new_node
            new_node.prev = n
            self.tail = new_node

    def addFirst(self,name,score):
        if not self.head:
            n = Node(name,score)
            self.head = n
            self.tail = n
        else:
            n = self.head
            # n = self.tail
            # while n.prev != None:
            #     n = n.prev
            new_node = Node(name,score)
            n.prev = new_node
            new_node.next = n
            self.head = new_node

    def addAt(self,name,score,pos):
      if not self.head:
            n = Node(name, score)
            self.head = n
            self.tail = n
      elif pos==1:
        self.AddFirst(name, score)
      else:
        new_node=Node(name, score)
        n=self.head
        cn=1                            # liczę od 1
        while n.next!=None and cn!=pos:
            n=n.next
            cn+=1
        if n.next==None:                # moglbym uzyc addLast ale tak jest szybciej
            n.next=new_node
            new_node.prev=n
            self.tail=new_node
        elif cn==pos:
            n.prev.next=new_node
            new_node.prev=n.prev
            n.prev=new_node
            new_node.next=n

    def delLast(self):
        n = self.tail
        n.prev.next = None
        self.tail = n.prev
        n.prev = None

    def delFirst(self):
        n = self.head
        n.next.prev = None
        self.head = n.next
        n.prev = None

    def printList(self):
        n = self.head
        while n:
            print n.name,n.score
            n = n.next

    def printListBackwards(self):
        n = self.tail
        while n:
            print n.name,n.score
            n = n.prev

    def bestScore(self):
        n = self.head
        best = n.score
        name = n.name
        while n.next:
            n=n.next
            if n.score>best:
                best = n.score
                name = n.name
        print "Najwiecej puntkow: ",best," uzyskal ",name




ll = UnidirectionalList()
ll.addLast("Snopek", 20)
ll.addLast("Szalska", 29.9)
ll.delFirst()
ll.addFirst("Nakielny", 20.4)
ll.addAt("Budzinski",24.5,2)
ll.addLast("Fugas", 20.5)
ll.printList()
ll.delLast()
print "\n"
ll.printListBackwards()
print "\n"
ll.bestScore()

# -*- coding: utf-8 -*-
"""
Created on Mon Nov 14 13:10:46 2016

@author: snoppiot
"""
import time
import random
#****************************************************************************
def babelkowe(lista):
    l = list(lista)
    dlugosc = len(l)
    while(dlugosc):
        zmiana = False
        for i in range(1,dlugosc):
            if(l[i-1]>l[i]):
                l[i-1], l[i] = l[i], l[i-1]
                zmiana = True
        if(not zmiana):
            return l
        dlugosc-=1


def wybieranie(lista):
    l = list(lista)
    dlugosc = len(l)
    for i in range(dlugosc):
        min = i
        for j in range(i,dlugosc-1):
            if(l[min]>l[j+1]):
                min = j+1

        l[i] , l[min] = l[min], l[i]
    return l


def wstawianie(lista):
    l=list(lista)
    dlugosc = len(l)
    for i in range(1,dlugosc):
        for j in range(0,i):
            if(l[i]<l[j]):
                l.insert(j,l.pop(i))
                break
    return l
def kopcowanie(lista):
    l = list(lista)
    dlugosc = len(l) - 1
    ojciec = dlugosc // 2
    for i in range ( ojciec, -1, -1 ):
        wdol( l, i, dlugosc )

    for i in range ( dlugosc, 0, -1 ):
        if l[0] > l[i]:
            l[0], l[i] = l[i], l[0]
            wdol( l, 0, i - 1 )
    return l
def wdol( l, pierwszy, ostatni ):
  najwiekszy = 2 * pierwszy + 1
  while(najwiekszy <= ostatni):
    if ( najwiekszy < ostatni ) and ( l[najwiekszy] < l[najwiekszy + 1] ):
      najwiekszy += 1

    if l[najwiekszy] > l[pierwszy]:
      l[najwiekszy], l[pierwszy] = l[pierwszy], l[najwiekszy]
      pierwszy = najwiekszy;
      najwiekszy = 2 * pierwszy + 1
    else:
      return

def szybkie(lista):
    l=list(lista)
    rowne = []
    wieksze = []
    mniejsze = []
    if len(l) > 1:
        pivot = l[int(len(l)/2)]
        for x in l:
            if x < pivot:
                mniejsze.append(x)
            if x == pivot:
                rowne.append(x)
            if x > pivot:
                wieksze.append(x)
        return szybkie(mniejsze)+rowne+szybkie(wieksze)
    else:
        return l


#***************************************************************************
random.seed()
lista = random.sample(list(range(50)),50)

print("Nieposortowane: " ,lista)

print("Babelkowe: " ,babelkowe(lista))

print("Wybieranie: " ,wybieranie(lista))

print("Wstawianie: " ,wstawianie(lista))

print("Kopcowanie: " ,kopcowanie(lista))

print("Szybkie: " ,szybkie(lista))
